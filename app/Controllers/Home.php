<?php

namespace App\Controllers;

class Home extends BaseController
{
	public function index()
	{
		$data = [
			"title" => 'Home',
			"isi" => 'v_home'
		];

		echo view('layout/v_wrapper', $data);
	}
	public function menu2()
	{
		$data = [
			"title" => 'Menu 2',
			"isi" => 'v_menu2'
		];

		echo view('layout/v_wrapper', $data);
	}
}
